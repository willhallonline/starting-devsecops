FROM python:latest

RUN mkdir /app

COPY ./app/python/ /app/

RUN cd /app && pip install -r requirements.txt

CMD ["python /app/flask.py"]
